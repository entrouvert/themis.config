from zope.interface import Interface
from zope import schema
from zope.i18nmessageid import MessageFactory

_ = MessageFactory(u'themis.config')

class IThemisSettings(Interface):
    '''Themis Settings'''

    mail_category_mapping = schema.Text(
        title=_(u'Mail Category Mapping'), required=False)

    documents_path = schema.TextLine(title=_(u'Documents Path'), required=False)

    incoming_mails_path = schema.TextLine(title=_(u'Incoming Mails Path'), required=False)
    outgoing_mails_path = schema.TextLine(title=_(u'Outgoing Mails Path'), required=False)
    internal_documents_path = schema.TextLine(title=_(u'Internal Documents Path'), required=False)
    confidential_documents_path = schema.TextLine(title=_(u'Confidential Documents Path'), required=False)

    ocr_code_mapping = schema.Text(title=_(u'OCR Code Mapping'), required=False)
    ocr_code_outgoing_mails_mapping = schema.Text(title=_(u'OCR Code Mapping (Outgoing Mails)'), required=False)
    ocr_code_internal_documents_mapping = schema.Text(title=_(u'OCR Code Mapping (Internal Documents)'), required=False)
    ocr_code_confidential_documents_mapping = schema.Text(title=_(u'OCR Code Mapping (Confidential Documents)'), required=False)
