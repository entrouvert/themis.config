import logging

from zope.component import getUtility
from zope.component import getMultiAdapter
from zope.publisher.browser import BrowserView

from plone.resource.utils import queryResourceDirectory

from plone.registry.interfaces import IRegistry

from themis.config.interfaces import _
from themis.config.interfaces import IThemisSettings

from AccessControl import Unauthorized
from Products.CMFCore.utils import getToolByName
from Products.Five.browser.decode import processInputs
from Products.statusmessages.interfaces import IStatusMessage

logger = logging.getLogger('themis.config')

class ThemisControlpanel(BrowserView):

    def __call__(self):
        if self.update():
            return self.index()
        return ''

    def _setup(self):
        self.settings = getUtility(IRegistry).forInterface(IThemisSettings, False)

    def update(self):
        processInputs(self.request)
        self._setup()
        self.errors = {}
        submitted = False
        form = self.request.form

        if 'form.button.Cancel' in form:
            self.redirect(_(u"Changes canceled."))
            return False

        if 'form.button.MailCategoryMappingSave' in form:
            self.authorize()
            submitted = True
            self.settings.mail_category_mapping = form.get('mail_category_mapping')
            self.settings.documents_path = form.get('documents_path')

        if 'form.button.OcrSave' in form:
            self.authorize()
            submitted = True

            self.settings.ocr_code_mapping = form.get('ocr_code_mapping')
            self.settings.ocr_code_outgoing_mails_mapping = form.get('ocr_code_outgoing_mails_mapping')
            self.settings.ocr_code_internal_documents_mapping = form.get('ocr_code_internal_documents_mapping')
            self.settings.ocr_code_confidential_documents_mapping = form.get('ocr_code_confidential_documents_mapping')
            self.settings.incoming_mails_path = form.get('incoming_mails_path')
            self.settings.outgoing_mails_path = form.get('outgoing_mails_path')
            self.settings.internal_documents_path = form.get('internal_documents_path')
            self.settings.confidential_documents_path = form.get('confidential_documents_path')

        if submitted and not self.errors:
            self._setup()
            IStatusMessage(self.request).add(_(u"Changes saved"))
        elif submitted:
            IStatusMessage(self.request).add(_(u"There were errors"), 'error')

        return True

    def authorize(self):
        authenticator = getMultiAdapter((self.context, self.request), name=u"authenticator")
        if not authenticator.verify():
            raise Unauthorized

    def redirect(self, message):
        IStatusMessage(self.request).add(message)
        portalUrl = getToolByName(self.context, 'portal_url')()
        self.request.response.redirect("%s/plone_control_panel" % portalUrl)
