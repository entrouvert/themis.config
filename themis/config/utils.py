from zope import component
from plone.registry.interfaces import IRegistry
from interfaces import IThemisSettings

def get_document_type_for_mail(mail):
    settings = component.getUtility(IRegistry).forInterface(IThemisSettings, False)
    mapping = settings.mail_category_mapping
    values = []
    if mail.categorie_de_courrier:
        values.extend(mail.categorie_de_courrier)
    if mail.sous_categorie_de_courrier:
        values.extend(mail.sous_categorie_de_courrier)
    for line in mapping.splitlines():
        if not line:
            continue
        if line.startswith('#'):
            continue
        category, object_type = line.strip().split('|')
        if category in values:
            return object_type
    return None

def get_document_location_for_mail(mail):
    settings = component.getUtility(IRegistry).forInterface(IThemisSettings, False)
    return settings.documents_path

def get_ocr_location(doctype):
    settings = component.getUtility(IRegistry).forInterface(IThemisSettings, False)
    return getattr(settings, doctype+'_path')

def get_categories_from_ocr_code(ocr_code, doctype):
    mapping_name = 'ocr_code_%s_mapping' % doctype
    if mapping_name == 'ocr_code_incoming_mails_mapping':
        mapping_name = 'ocr_code_mapping'
    settings = component.getUtility(IRegistry).forInterface(IThemisSettings, False)
    mapping = getattr(settings, mapping_name)
    if mapping is None:
        mapping = ''
    for line in mapping.splitlines():
        if not line:
            continue
        if line.startswith('#'):
            continue
        if line.count('|') == 2:
            code, category, subcategory = line.strip().split('|')
            folder = None
        if line.count('|') == 3:
            code, category, subcategory, folder = line.strip().split('|')
            subcategory = subcategory or None
        elif line.count('|') == 1:
            code, category = line.strip().split('|')
            subcategory, folder = None, None
        if code == ocr_code:
            return category, subcategory, folder
    return None

